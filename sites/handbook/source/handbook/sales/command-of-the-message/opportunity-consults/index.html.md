---
layout: handbook-page-toc
title: "Opportunity Consults"
description: "Opportunity consults are deal reviews that focus on helping a sales team member maximize the likelihood of winning and securing the customer's business"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Opportunity consults are deal reviews that focus on helping a sales team member maximize the likelihood of winning and securing the customer's business. They are based on a joint inspection of the "health" of the opportunity's [Command Plan](/handbook/sales/command-of-the-message/command-plan/) and a 2-way dialogue to identify and commit to specific actions that aim to increase the likelihood of success.

## Opportunity Consult Format
Below is a general recommendation of the format and flow of an opportunity consult. 

| **Step (duration)** | **Description** |
| ------ | ------ |
| Manager Set-Up (5 minutes) | - Set expectations of the call by stating the purpose, process and payoff of the session |
| Seller-Led Opportunity Overview (10 minutes uninterrupted) | - Job aid: [Opportunity Qualifier](https://docs.google.com/document/d/1Tz6bQKD4Ff2-XqpSXRQslD8yvrphwXaL6oEl74DAjeQ/edit?usp=sharing) <br> - Provides brief opportunity background (e.g., relevant account history, recent changes on opportunity) <br> - Describes current deal state (e.g., sales process stage, forecast stage, key business issues, compelling event) <br> - Identifies gaps in Mantra, [MEDDPPICC](/handbook/sales/meddppicc/), and additional challenges for discussion |
| Manager Consult (30 minutes) | - Job aid: [Opportunity Coaching Guide](https://docs.google.com/document/d/1IZA9Fo2SvZOrtUVpXOjwwqs76lKdXFs4hTezbxRq5v8/edit?usp=sharing) <br> - Ask open-ended, two-sided questions around focus areas to inspect (in a good, helpful way) the opportunity <br> - Provide relevant feedback and coaching based on the identified gaps and challenges <br> - Assist the seller in defining an action plan and exactly “how” to execute on the next steps |
| Manager + Seller Agree to Action Plan (5 minutes) | - Seller recaps opportunity-specific sales strategy <br> - Seller documents next steps/critical actions in CRM or via email to manager |

## Best Practices

- Conduct regular weekly Opportunity Consults with each rep on your team to review Command Plans for strategic opportunities
- Focus on continually driving higher fidelity Command Plans through each customer communication/interaction
- Coaches/managers should first seek to understand the current level of understanding of those they are coaching before explaining key or misunderstood concepts
- Coaches/managers should encourage reps to
    - Commit to specific, concrete, actionable next steps (i.e., “I am going to do X, Y, and Z to test my Champion” instead of “I am going to test my Champion”) and
    - Document action items in a shared way (i.e. document in Salesforce or send a summary wrap-up communication after the meeting)
- Include key account team members (e.g. SAs and TAMs) in Opportunity Consults to facilitate improved transparency and collaboration
- Coach sales team members on the best practices related to the Close Plan within a [Command Plan](/handbook/sales/command-of-the-message/command-plan/)
    - Close plans should be updated in real time and, at minimum, on a weekly basis
    - Time stamp the latest update at the top with the date of the refresh so each line item starts with a specific date
    - Liberally use ( ) or ** ** between line items or at the end of line items to denote Close Plan gaps that the sales team member intends to fill during the next communication with the prospect/customer
    - Keep documented actions as crisp and succinct as possible; endeavor to keep it on one line so as not to make it a blog
    - Refer to the Paper Process when appropriate so efforts aren't duplicated

## Additional Resources

Click on the "Sales Manager Materials" drop down in the [Command of the Message Additional Resources](/handbook/sales/command-of-the-message/#additional-resources) section.
