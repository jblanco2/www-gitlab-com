---
layout: markdown_page
title: "DevOps Platform Message House"
---



| Positioning Statement: | *(how GitLab fits and is differentiated in the market for this use case)* |
|------------------------|-------------------------------------------------------------------------|
| **Short Description** | A complete DevOps platform deployed as a single application eliminates integration bottlenecks, enables end-to-end transparency, and allows businesses to deliver higher quality, more secure software more efficiently. |
| **Long Description** | GitLab is a complete DevOps platform deployed as a single application, reducing cycle time and making teams more efficient. GitLab eliminates the overhead and cost of integrating and managing complex toolchains, allowing you to focus teams on creating business value. One underlying data store enables end-to-end transparency so every stakeholder—from portfolio managers to developers to security experts to external auditors and customers—can collaborate in the development process and understand exactly what and why changes were made, by whom, and how they affected product quality, security, and compliance. One application means greater efficiency, better security, higher quality, and more value delivered for the same time and cost. |


| **Key-Values** | Value 1: *<List a key message/ value proposition>* | Value 2: | Value 3: |
|--------------|------------------------------------------------------------------|----------|----------|
| **Promise** | Identify and address workflow blockers and bottlenecks to help teams work more efficiently. | Focus on value delivery—not infrastructure. | Reduce security and compliance risks without compromising speed or increasing cost. |
| **Pain points** | - struggling to measure efficiency and understand where processes break down<br> - moving from analysis to action and remediating process breakdowns<br> - correlating process changes with efficiency outcomes<br> - lack of consistent measurement across projects and teams |  - wasting time and resources maintaining toolchain integrations<br> - silos of tool-specific competentcies<br> - "data gaps" in integrations resulting in incomplete context and manual workarounds | - remidiating vulnerabilities and non-compliance late in the development process is expensive and time-consuming<br> - some vulnerabilities only discoverable on live sites<br> - forensics and audits create an enormous burden across the entire team |
| **Why GitLab** | As a complete end-to-end DevOps platform built on a single data store, GitLab can provide complete visibility into all of your work, including requirements creation, planning the work, code changes, security and quality reviews, deplopyments, and all collaboration throught the process. This enables out-of-the-box analytics that can be customized to your workflows, and a variety of different dashboard that surface relevant information to specific roles within your organization, while allowing everyone to actively contribute with just a few clicks. | A single application eliminates the need to maintain brittle integrations. A shared data store allows any user in any stage of the development process to access data from any other stage. Data import and export apply to all of your DevSecOps life cycle. | Vulnerability and compliance scans run with each commit, allowing the developer to find and fix them early, while improving their security awareness. The Security Dashboard provides insights security pros need, showing remaining vulnerabilities across projects and/or groups, along with actions taken, by whom and when. All actions—from planning to code changes to approvals—are captured and correlated for easy traceability during audit events. |


| **Proof points** | *(list specific analyst reports, case studies, testimonials, etc)*  |

- [Glympse case study](/customers/glympse/)
(~20 tools consolidated into GitLab; Glympse remediated security issues faster than any other company in their Security Auditor's experience)

> "Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts."<br>**Zaq Wiedmann**<br>Lead Software Engineer, Glympse

***

- [BI Worldwide case study](/customers/bi_worldwide/)
(BI Worldwide performed 300 SAST/Dependency scans in the first 4 days of use, helping the team identify previously unidentified vulnerabilities.)

> "One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role."<br>Adam Dehnel<br>Product architect, BI Worldwide

***

- [Gartner 2020 Market Guide for DevOps Value Stream Delivery Platforms](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html)

***
