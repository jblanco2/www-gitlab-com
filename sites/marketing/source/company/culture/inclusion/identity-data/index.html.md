---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2020-10-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 136       | 10.79%          |
| Based in EMEA                               | 337       | 26.72%          |
| Based in LATAM                              | 20        | 1.59%           |
| Based in NORAM                              | 768       | 60.90%          |
| **Total Team Members**                      | **1,261** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 874       | 69.31%          |
| Women                                       | 387       | 30.69%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,261** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 156       | 68.12%          |
| Women in Management                         | 73        | 31.88%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **229**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 74        | 74.75%          |
| Women in Leadership                         | 25        | 25.25%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **99**    | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 434       | 80.37%          |
| Women in Engineering                        | 106       | 19.63%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **540**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.28%           |
| Asian                                       | 47        | 6.53%           |
| Black or African American                   | 18        | 2.50%           |
| Hispanic or Latino                          | 38        | 5.28%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 32        | 4.44%           |
| White                                       | 442       | 61.39%          |
| Unreported                                  | 141       | 19.58%          |
| **Total Team Members**                      | **720**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 17        | 8.06%           |
| Black or African American                   | 3         | 1.42%           |
| Hispanic or Latino                          | 7         | 3.32%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.27%           |
| White                                       | 139       | 65.88%          |
| Unreported                                  | 36        | 17.06%          |
| **Total Team Members**                      | **211**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 5.44%           |
| Black or African American                   | 3         | 2.04%           |
| Hispanic or Latino                          | 4         | 2.72%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 4.76%           |
| White                                       | 91        | 61.90%          |
| Unreported                                  | 34        | 23.13%          |
| **Total Team Members**                      | **147**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 11.54%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 5.13%           |
| White                                       | 49        | 62.82%          |
| Unreported                                  | 16        | 20.51%          |
| **Total Team Members**                      | **78**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 117       | 9.28%           |
| Black or African American                   | 30        | 2.38%           |
| Hispanic or Latino                          | 66        | 5.23%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.08%           |
| Two or More Races                           | 40        | 3.17%           |
| White                                       | 708       | 56.15%          |
| Unreported                                  | 297       | 23.55%          |
| **Total Team Members**                      | **1,261** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 57        | 10.56%          |
| Black or African American                   | 9         | 1.67%           |
| Hispanic or Latino                          | 26        | 4.81%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.19%           |
| Two or More Races                           | 15        | 2.78%           |
| White                                       | 304       | 56.30%          |
| Unreported                                  | 128       | 23.70%          |
| **Total Team Members**                      | **540**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 15        | 6.55%           |
| Black or African American                   | 3         | 1.31%           |
| Hispanic or Latino                          | 6         | 2.62%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 3.49%           |
| White                                       | 142       | 62.01%          |
| Unreported                                  | 55        | 24.02%          |
| **Total Team Members**                      | **229**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 10.10%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.01%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 4.04%           |
| White                                       | 59        | 59.60%          |
| Unreported                                  | 25        | 25.25%          |
| **Total Team Members**                      | **99**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 15        | 1.19%           |
| 25-29                                       | 208       | 16.49%          |
| 30-34                                       | 346       | 27.44%          |
| 35-39                                       | 283       | 22.44%          |
| 40-49                                       | 279       | 22.13%          |
| 50-59                                       | 115       | 9.12%           |
| 60+                                         | 15        | 1.19%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,261** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
